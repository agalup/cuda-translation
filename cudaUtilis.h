#include "cuda.h"

CUmodule cudaInitialization(CUcontext* cuContext);

void allocateHostMemory(int*& active_nodes, int*& up, int*& node, int*& left, 
        int*& right, int size);

void allocateDeviceMemory(  CUdeviceptr* d_leaves, int* leaves, 
                            CUdeviceptr* d_active_nodes, int* active_nodes,
                            CUdeviceptr* d_up, int* up,
                            CUdeviceptr* d_node, int* node,
                            CUdeviceptr* d_left, int* left,
                            CUdeviceptr* d_right, int* right, 
                            int size, int number_of_leaves);

void debug_totalMemoryUse(int size, int numberOfLeaves, int real_size);

void copyBackToHost(CUdeviceptr d_up, int* up, CUdeviceptr d_node, int* node, 
        CUdeviceptr d_left, int* left, CUdeviceptr d_right, int* right, int real_size);

void cleaning(  CUdeviceptr* d_leaves, int* leaves, 
                CUdeviceptr* d_active_nodes, int* active_nodes,
                CUdeviceptr* d_up, int* up,
                CUdeviceptr* d_node, int* node,
                CUdeviceptr* d_left, int* left,
                CUdeviceptr* d_right, int* right,
                CUcontext* cuContext);
