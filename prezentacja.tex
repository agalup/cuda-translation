%\documentclass[a4paper, twocolumn, 11pt]{article}
%\documentclass[a4paper, 11pt]{article}
\documentclass[10pt]{beamer}
\beamertemplatenavigationsymbolsempty
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{latexsym}
\usepackage{amssymb,amsmath,amsthm}
\usepackage{graphicx}
\usepackage{pstricks}
\usepackage{wrapfig}
\usepackage{subcaption}
\usepackage{color}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{pst-node, pst-tree}
\newcommand*\Let[2]{\State #1 $\gets$ #2}

\newtheorem{tw}{Theory}[section]
\newtheorem{df}[tw]{Definition}
\newtheorem{notation}[tw]{Notation}
\newtheorem{lm}[tw]{Lemma}
\newtheorem{obs}[tw]{Observation}

\newenvironment{pr}[1][Property]{\begin{trivlist}
\item[\hskip \labelsep {\bfseries #1}]}{\end{trivlist}}

\usepackage{indentfirst}

\title{\bf Parallel standard translation between Lambda Calculus and 
Combinatory Logic}
\author{Agnieszka Lupinska\\
agnieszka.lupinska [at] uj.edu.pl\\
Theoretical Computer Science\\
Jagiellonian University}
\date{}

\psset{levelsep=1cm,nodesep=3pt}

\begin{document}

\begin{frame}
    \titlepage
\end{frame}

\begin{frame}
\frametitle{Lambda Calculus}

The Lambda Calculus, denotes by $\lambda$, is a formal system in mathematical 
logic, consists of a language of $\lambda$-terms. $\lambda$-terms are defined 
by the following syntax and the set of transformation rules.

\begin{df}
Let $\{x_1, x_2, \dots \}$ be an infinite set of variables. 
\begin{enumerate}
\item Each variable $x_i$ is a $\lambda$-term.
\item If $x$ is a variable and $T$ is a $\lambda$-term, then $\lambda x.T$ is 
a $\lambda$-term, called \textbf{abstraction}. It represents a one-argument 
function which takes an input $x$ and returns $T$
\item If $S$ and $T$ are $\lambda$-terms then $ST$ is a $\lambda$-term, called 
\textbf{application}. It represents an application function $S$ to an input $T$.
\end{enumerate}
\end{df}
\end{frame}

\begin{frame}
\frametitle{Notation}

\begin{notation}
\begin{enumerate}
\item Let $\vec{x} \equiv x_1, \dots x_n$. Then\\ $\lambda x_1 x_2 \dots x_n.T 
\equiv \lambda \vec{x}.T \equiv \lambda x_1.\big(\lambda x_2.\big( \dots \big(
\lambda x_n.T \big) \ldots \big)\big)$

\item Let $\vec{T} \equiv T_1, \dots T_n$. Then\\ $ST_1T_2 \dots T_n \equiv 
\vec{T}S \equiv \big( \dots \big( \big(ST_1\big) T_2 \big) \dots T_n \big)$
\end{enumerate}
\end{notation}

\textbf{Examples}. 
\begin{center}
\begin{tabular}{l}
 $xy$\\
 $xyz \equiv ((xy)z)$\\
 $\lambda x.x$\\
 $\lambda x.xx$\\
 $\lambda xy.xy \equiv \lambda x.\big(\lambda y.xy\big)$ \\
 $\lambda yx.yx \equiv \lambda y.\big(\lambda x.yx\big)$ \\
 $\lambda xy.x \equiv \lambda x.\big(\lambda y.x\big)$\\
 $\lambda xyz.xz(yz) \equiv \lambda x.\big(\lambda y.\big(\lambda z.xz(yz)
\big)\big)$
\end{tabular}
\end{center}

\end{frame}

\begin{frame}

\frametitle{Free variables}
\begin{df}
A variable $x$ is \textbf{free} in a $\lambda$-term $S$ if $x$ is not in 
the scope of a $\lambda$x. \textbf{FV($S$)} is the set of all free variables 
in $S$, defined inductively:
\begin{center}
\begin{tabular}{l}
$FV(x) = x$\\
$FV(\lambda x.S) = FV(S) - \{x\}$\\
$FV(ST) = FV(S) \cup FV(T)$
\end{tabular}
\end{center}
\end{df}

\end{frame}

\begin{frame}

\frametitle{$\lambda$-tree construction.}

In a graph theory 
$\lambda$-terms can be represented by trees in the following way. A 
$\lambda$-abstraction of variable $x$ is an unary node with the label 
$\lambda x$. An application $ST$ is a binary node with the label $@$ and 
left child $S$ and rigth child $T$. A variable $x$ is a leaf with the label $x$.\\

\textbf{Examples}.\\
For a given $\lambda$-term $\lambda x.xx$ the 
$\lambda$-tree is:
\begin{center}
\begin{psTree}{\Tr{$\lambda x$}}
\begin{psTree}{\Tr{$@$}}
\Tr{$x$}
\Tr{$x$}
\end{psTree}
\end{psTree}
\end{center}

\end{frame}

\begin{frame}
\frametitle{Examples}. 

For a given $\lambda$-term $\lambda xyz.xz(yz)$ the 
$\lambda$-tree is:

\begin{center}
\begin{psTree}{\Tr{$\lambda$x}}
\begin{psTree}{\Tr{$\lambda$y}}
\begin{psTree}{\Tr{$\lambda$z}}
\begin{psTree}{\Tr{$@$}}
\begin{psTree}{\Tr{$@$}}
\Tr{$x$}
\Tr{$z$}
\end{psTree}
\begin{psTree}{\Tr{$@$}}
\Tr{$y$}
\Tr{$z$}
\end{psTree}
\end{psTree}
\end{psTree}
\end{psTree}
\end{psTree}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Size of $\lambda$-term}
\begin{df}
The size of $\lambda$-term is defined by induction in the following way:
\begin{center}
%\begin{tabular}{l}
$|x| = 1$\\
$|\lambda x.M| = 1 + |M|$\\
$|MN| = |M| + |N|$
%\end{tabular}
\end{center}
\end{df}
\end{frame}

\begin{frame}
\frametitle{Combinatory Logic}
Combinatory Logic, denoted $\mathbf{CL}$, is a theoretical model of 
computation with the axioms $\mathbf{K}$, $\mathbf{S}$. Let $P, Q, R \in 
\mathbf{CL}$, then: 
\begin{center}
\begin{tabular}{l}
    $\mathbf{K}PQ = P$\\
    $\mathbf{S}PQR = PR(QR)$
\end{tabular}
\end{center}
Elements of $\mathbf{CL}$ are called combinators. 
We are given the following 
rules.

\begin{df}
Let $\{x_1, x_2, \dots \}$ be an infinite set of variables.
\begin{center}
\begin{tabular}{l}
Each variable $x_i$ is in $\mathbf{CL}$\\
Axioms: $\mathbf{K} \in \mathbf{CL}$ and $\mathbf{S} \in \mathbf{CL}$\\
Application operation: $P, Q \in \mathbf{CL}$, then $PQ \in \mathbf{CL}$
\end{tabular}
\end{center}
\end{df}
\end{frame}

\begin{frame}
\frametitle{Combinator tree construction}
In a graph theory combinator can be represented by trees in the 
following way. An application is a binary node. A variable, $S$ and $K$
are leaves.\\

\textbf{Examples}. For a given combinator $SKK$ the tree is:
\begin{center}
\begin{psTree}{\Tr{}}
\begin{psTree}{\Tr{}}
\Tr{$S$}
\Tr{$K$}
\end{psTree}
\Tr{$K$}
\end{psTree}
\end{center}

\begin{df}
The size of combinator is defined by induction in the following way:
\begin{center}
$|x| = |S| = |K| = 1$\\
$|MN| = |M| + |N| + 1$
\end{center}
\end{df}

\begin{df}
For a combinator $P$, set \textbf{FV}($P$) denotes all variable in $P$.
%$P[x:=Q]$ denotes the result of substituting $Q$ for all occurances $x$ in $P$
\end{df}
\end{frame}


\begin{frame}
\frametitle{Simulation $\lambda$-abstraction by $\mathbf{K}$ and $\mathbf{S}$}.

\begin{lm}
Define $\mathbf{I} := \mathbf{SKK}$. For a combinator $P$ there is 
$\mathbf{CL} \vdash \mathbf{I}P = P$
\end{lm}
\textbf{Proof}: $\mathbf{I}P \equiv \mathbf{SKK}P = \mathbf{K}P(\mathbf{K}P) = P$

\begin{df}
We define $\lambda^* x.P$ by induction on the structure $P$ as follows:
\begin{center}
\begin{tabular}{l}
$\lambda ^* x.x \equiv I$\\
$\lambda ^* x.P \equiv \mathbf{K}P$ if $x \notin \text{FV}(P)$\\
$\lambda ^* x.PQ \equiv \mathbf{S}(\lambda^*x.P)(\lambda^*x.Q)$
\end{tabular}
\end{center}
\end{df}

\textbf{Notation}\\
Let $\vec{x} \equiv x_1, \dots x_n$.\\ Then $\lambda^* x_1 x_2 \dots x_n.T 
\equiv \lambda^* \vec{x}.T \equiv \lambda^* x_1.\big(\lambda^* x_2.\big( \dots 
\big(\lambda^* x_n.T \big) \ldots \big)\big)$

\textbf{Example}\\
$\lambda^*xy.yx \equiv \lambda^*x.(\lambda^*y.yx) \equiv 
\lambda^*x.\mathbf{S}(\lambda^*y.y)(\lambda^*y.x) \equiv
\lambda^*x.\mathbf{SI}(\mathbf{K}x) \equiv \mathbf{S(K(SI))(S(KK)I)}$
\end{frame}

\begin{frame}
\frametitle{Standard translation}

\begin{df}

Standard translation from Combinatory Logic to Lambda Calculus:

\begin{center}
\begin{tabular}{l}
$[x]_{\lambda} \equiv x$\\
$[\mathbf{K}]_{\lambda} \equiv \lambda xy.x$\\
$[\mathbf{S}]_{\lambda} \equiv \lambda xyz.xz(yz)$\\
$[PQ]_{\lambda} \equiv [P]_{\lambda}[Q]_{\lambda}$
\end{tabular}
\end{center}

Standard translation from Lambda Calculus to Combinatory Logic:

\begin{center}
\begin{tabular}{l}
$[x]_{CL} \equiv x$\\
$[MN]_{CL} \equiv [M]_{CL}[N]_{CL}$\\
$[\lambda x.M]_{CL} \equiv \lambda^* x.[M]_{CL}$
\end{tabular}
\end{center}
\end{df}
\end{frame}
\begin{frame}

\frametitle{Example}

Translation $\lambda$-term $\lambda x.xx$ 
to combinator\\
$$[\lambda x.xx]_{CL} \equiv \lambda^*x.[xx]_{CL} = \lambda^*x.xx = 
S(\lambda^*x.x)(\lambda^*x.x) = SII$$

\begin{figure}[H]
\centering
\begin{psTree}{\Tr{$\lambda x$}}
\begin{psTree}{\Tr{$@$}}
\Tr{$x$}
\Tr{$x$}
\end{psTree}
\end{psTree}
\caption{$\lambda$-term: $\lambda x.xx$}
\end{figure}

\begin{figure}[H]
\centering
\begin{psTree}{\Tr{$\lambda^*x$}}
\begin{psTree}{\Tr{}}
\Tr{$x$}
\Tr{$x$}
\end{psTree}
\end{psTree}
\caption{After translation we have got the abstraction in CL: 
$\lambda^*x.xx$}
\end{figure}

\end{frame}
\begin{frame}

\begin{figure}[H]
\centering
\begin{psTree}{\Tr{}}
\begin{psTree}{\Tr{}}
\Tr{$S$}
\begin{psTree}{\Tr{$\lambda^*x$}}
\Tr{$x$}
\end{psTree}
\end{psTree}
\begin{psTree}{\Tr{$\lambda^*x$}}
\Tr{$x$}
\end{psTree}
\end{psTree}
\caption{From definition of the abstraction.}
\end{figure}

\begin{figure}[H]
\centering
\begin{psTree}{\Tr{}}
\begin{psTree}{\Tr{}}
\Tr{$S$}
\Tr{$I$}
\end{psTree}
\Tr{$I$}
\end{psTree}
\caption{Since $\lambda^*x.x = I$}
\end{figure}
\end{frame}

\begin{frame}

\frametitle{Example}
Translation $\lambda$-term 
$\lambda xy.x(y \lambda z.z)$ to CL-combinator

\begin{figure}[H]
\centering
\begin{psTree}{\Tr{$\lambda x$}}
\begin{psTree}{\Tr{$\lambda y$}}
\begin{psTree}{\Tr{$@$}}
\Tr{$x$}
\begin{psTree}{\Tr{$@$}}
\Tr{$y$}
\begin{psTree}{\Tr{$\lambda z$}}
\Tr{$z$}
\end{psTree}
\end{psTree}
\end{psTree}
\end{psTree}
\end{psTree}
\caption{$\lambda$-term: $\lambda xy.x(y \lambda z.z)$}
\end{figure}

\end{frame}
\begin{frame}
From definition of translation to CL:
\begin{center}
\begin{tabular}{l}
$[\lambda xy.x(y \lambda z.z)]_{CL} \equiv$ \\
$\lambda^*xy.[x(y\lambda z.z)]_{CL} \equiv$ \\
$\lambda^*xy.[x]_{CL}[y\lambda z.z]_{CL} \equiv$ \\
$\lambda^*xy.x([y]_{CL}[\lambda z.z]_{CL}) \equiv$ \\ 
$\lambda^*xy.x(y\lambda^*z.z)$
\end{tabular}
\end{center}

\begin{figure}[H]
\centering
\begin{psTree}{\Tr{$\lambda^* x$}}
\begin{psTree}{\Tr{$\lambda^* y$}}
\begin{psTree}{\Tr{}}
\Tr{$x$}
\begin{psTree}{\Tr{}}
\Tr{$y$}
\begin{psTree}{\Tr{$\lambda^* z$}}
\Tr{$z$}
\end{psTree}
\end{psTree}
\end{psTree}
\end{psTree}
\end{psTree}
\caption{After translation to CL}
\end{figure}

\end{frame}
\begin{frame}
From definition of abstraction in CL:
\begin{center}
\begin{tabular}{l}
$\lambda^*xy.x(y\lambda^*z.z) = \lambda^*xy.x(yI)$
\end{tabular}
\end{center}

\begin{figure}[H]
\centering
\begin{psTree}{\Tr{$\lambda^* x$}}
\begin{psTree}{\Tr{$\lambda^* y$}}
\begin{psTree}{\Tr{}}
\Tr{$x$}
\begin{psTree}{\Tr{}}
\Tr{$y$}
\Tr{$I$}
\end{psTree}
\end{psTree}
\end{psTree}
\end{psTree}
\caption{After removing the first $\lambda^*$ abstraction $z$}
\end{figure}

\end{frame}
\begin{frame}
\begin{center}
\begin{tabular}{l}
$\lambda^*xy.x(yI) = \lambda^*x.\lambda^*y.x(yI) = 
\lambda^*x.S(\lambda^*y.x)(\lambda^*y.yI)$
\end{tabular}
\end{center}

\begin{figure}[H]
\centering
\begin{psTree}{\Tr{$\lambda^* x$}}
\begin{psTree}{\Tr{}}
\begin{psTree}{\Tr{}}
\Tr{$S$}
\begin{psTree}{\Tr{$\lambda^* y$}}
\Tr{$x$}
\end{psTree}
\end{psTree}
\begin{psTree}{\Tr{$\lambda^* y$}}
\Tr{$y$}
\Tr{$I$}
\end{psTree}
\end{psTree}
\end{psTree}
\end{figure}

\end{frame}
\begin{frame}

\begin{center}
\begin{tabular}{l}
$\lambda^*x.S(\lambda^*y.x)(\lambda^*y.yI) =
\lambda^*x.S(Kx)(S(\lambda^*y.y)(\lambda^*y.I))$
\end{tabular}
\end{center}

\begin{figure}[H]
\centering
\begin{psTree}{\Tr{$\lambda^* x$}}
\begin{psTree}{\Tr{}}
\begin{psTree}{\Tr{}}
\Tr{$S$}
\begin{psTree}{\Tr{}}
\Tr{$K$}
\Tr{$x$}
\end{psTree}
\end{psTree}
\begin{psTree}{\Tr{}}
\begin{psTree}{\Tr{}}
\Tr{$S$}
\begin{psTree}{\Tr{$\lambda^* y$}}
\Tr{$y$}
\end{psTree}
\end{psTree}
\begin{psTree}{\Tr{$\lambda^* y$}}
\Tr{$I$}
\end{psTree}
\end{psTree}
\end{psTree}
\end{psTree}
\end{figure}

\end{frame}
\begin{frame}

\begin{center}
\begin{tabular}{l}
$\lambda^*x.S(Kx)(S(\lambda^*y.y)(\lambda^*y.I)) = \lambda^*x.S(Kx)(SI(KI))$
\end{tabular}
\end{center}

\begin{figure}[H]
\centering
\begin{psTree}{\Tr{$\lambda^* x$}}
\begin{psTree}{\Tr{}}
\begin{psTree}{\Tr{}}
\Tr{$S$}
\begin{psTree}{\Tr{}}
\Tr{$K$}
\Tr{$x$}
\end{psTree}
\end{psTree}
\begin{psTree}{\Tr{}}
\begin{psTree}{\Tr{}}
\Tr{$S$}
\Tr{$I$}
\end{psTree}
\begin{psTree}{\Tr{}}
\Tr{$K$}
\Tr{$I$}
\end{psTree}
\end{psTree}
\end{psTree}
\end{psTree}
\caption{After removing the second $\lambda^*$ abstraction $y$}
\end{figure}

\end{frame}

\begin{frame}

\begin{center}
\begin{tabular}{l}
$\lambda^*x.S(Kx)(SI(KI)) = S(\lambda^*x.S(Kx))(\lambda^*x.SI(KI))$\\
\end{tabular}
\end{center}

\begin{figure}[H]
\centering
\begin{psTree}{\Tr{}}
    \begin{psTree}{\Tr{}}
	\Tr{$S$}
	\begin{psTree}{\Tr{$\lambda^*x$}}
		\begin{psTree}{\Tr{}}
		    \Tr{$S$}
		    \begin{psTree}{\Tr{}}
			\Tr{$K$}
			\Tr{$x$}
		    \end{psTree}
		\end{psTree}
	\end{psTree}
    \end{psTree}
    \begin{psTree}{\Tr{$\lambda^*x$}}
	\begin{psTree}{\Tr{}}
	\begin{psTree}{\Tr{}}
	    \Tr{$S$}
	    \Tr{$I$}
	\end{psTree}
        \begin{psTree}{\Tr{}}
	    \Tr{$K$}
	    \Tr{$I$}
	\end{psTree}
	\end{psTree}
    \end{psTree}
\end{psTree}
\end{figure}
\end{frame}
\begin{frame}
\begin{center}
\begin{tabular}{l}
$S(\lambda^*x.S(Kx))(\lambda^*x.SI(KI)) = 
S(S(\lambda^*x.S)(\lambda^*x.Kx))(K(SI(KI)))$
\end{tabular}
\end{center}

\begin{figure}[H]
\centering
\begin{psTree}{\Tr{}}
    \begin{psTree}{\Tr{}}
	\Tr{$S$}
	\begin{psTree}{\Tr{}}
	    \begin{psTree}{\Tr{}}
		\Tr{$S$}
		\begin{psTree}{\Tr{$\lambda^*x$}}
		    \Tr{$S$}
		\end{psTree}
	    \end{psTree}
	    \begin{psTree}{\Tr{$\lambda^*x$}}
		\begin{psTree}{\Tr{}}
	    	\Tr{$K$}
    		\Tr{$x$}
		\end{psTree}
	    \end{psTree}
	\end{psTree}
    \end{psTree}
    \begin{psTree}{\Tr{}}
	\Tr{$K$}
	\begin{psTree}{\Tr{}}
	\begin{psTree}{\Tr{}}
	    \Tr{$S$}
	    \Tr{$I$}
	\end{psTree}
        \begin{psTree}{\Tr{}}
	    \Tr{$K$}
	    \Tr{$I$}
	\end{psTree}
	\end{psTree}
    \end{psTree}
\end{psTree}
\end{figure}

\end{frame}
\begin{frame}
\begin{center}
\begin{tabular}{l}
$S(S(\lambda^*x.S)(\lambda^*x.Kx))(K(SI(KI))) = 
 S(S(KS)(S(\lambda^*x.K)(\lambda^*x.x))(K(SI(KI)))$
\end{tabular}
\end{center}

\begin{figure}[H]
\centering
\begin{psTree}{\Tr{}}
    \begin{psTree}{\Tr{}}
	\Tr{$S$}
	\begin{psTree}{\Tr{}}
	    \begin{psTree}{\Tr{}}
		\Tr{$S$}
		\begin{psTree}{\Tr{}}
		    \Tr{$K$}
		    \Tr{$S$}
		\end{psTree}
	    \end{psTree}
	    \begin{psTree}{\Tr{}}
		\begin{psTree}{\Tr{}}
		    \Tr{$S$}
		    \begin{psTree}{\Tr{$\lambda^*x$}}
			\Tr{$K$}
		    \end{psTree}
		\end{psTree}
		\begin{psTree}{\Tr{$\lambda^*x$}}
		    \Tr{$x$}
		\end{psTree}	
	    \end{psTree}
	\end{psTree}
    \end{psTree}
    \begin{psTree}{\Tr{}}
	\Tr{$K$}
	\begin{psTree}{\Tr{}}
	\begin{psTree}{\Tr{}}
	    \Tr{$S$}
	    \Tr{$I$}
	\end{psTree}
        \begin{psTree}{\Tr{}}
	    \Tr{$K$}
	    \Tr{$I$}
	\end{psTree}
	\end{psTree}
    \end{psTree}
\end{psTree}
\end{figure}

\end{frame}
\begin{frame}
\begin{center}
\begin{tabular}{l}
$S(S(KS)(S(\lambda^*x.K)(\lambda^*x.x))(K(SI(KI))) = $
$S(S(KS)(S(KK)I))(K(SI(KI)))$
\end{tabular}
\end{center}

\begin{figure}[H]
\centering
\begin{psTree}{\Tr{}}
    \begin{psTree}{\Tr{}}
	\Tr{$S$}
	\begin{psTree}{\Tr{}}
	    \begin{psTree}{\Tr{}}
		\Tr{$S$}
		\begin{psTree}{\Tr{}}
		    \Tr{$K$}
		    \Tr{$S$}
		\end{psTree}
	    \end{psTree}
	    \begin{psTree}{\Tr{}}
		\begin{psTree}{\Tr{}}
		    \Tr{$S$}
		    \begin{psTree}{\Tr{}}
			\Tr{$K$}
			\Tr{$K$}
		    \end{psTree}
		\end{psTree}
		\Tr{$I$}
	    \end{psTree}
	\end{psTree}
    \end{psTree}
    \begin{psTree}{\Tr{}}
	\Tr{$K$}
	\begin{psTree}{\Tr{}}
	\begin{psTree}{\Tr{}}
	    \Tr{$S$}
	    \Tr{$I$}
	\end{psTree}
        \begin{psTree}{\Tr{}}
	    \Tr{$K$}
	    \Tr{$I$}
	\end{psTree}
	\end{psTree}
    \end{psTree}
\end{psTree}
\end{figure}

Summary we have got 
$\lambda xy.x(y \lambda z.z) = S(S(KS)(S(KK)I))(K(SI(KI)))$

\end{frame}
%pare slow o wielkosci termu po przetlumaczeniu. trzeba wprowadzic
%miare lambda termu i wspomniec o lukasza pracy

\begin{frame}

1. For each $\lambda^*x$-abstraction a tree is rebuild only in 
nodes which lie on the paths leading from leaves $x$ to the root.

\begin{psTree}{\Tr{$\lambda^* x$}}
    \begin{psTree}{\Tcircle{3}}
	\begin{psTree}{\Tcircle{2}}
	    \Tr{$S$}
	    \begin{psTree}{\Tcircle{1}}
		\Tr{$K$}
		\Tcircle{$x$}
	    \end{psTree}
	\end{psTree}
	\begin{psTree}{\Tr{}}
	    \begin{psTree}{\Tr{}}
		\Tr{$S$}
		\Tr{$I$}
	    \end{psTree}
	    \begin{psTree}{\Tr{}}
		\Tr{$K$}
		\Tr{$I$}
	    \end{psTree}
	\end{psTree}
    \end{psTree}
\end{psTree}
\begin{psTree}{\Tcircle{3}}
    \begin{psTree}{\psset{linestyle=dashed}\Tr{}}
	{\psset{linestyle=dashed}\Tr{$S$}}
	\begin{psTree}{\Tcircle{2}}
	    \begin{psTree}{\psset{linestyle=dashed}\Tr{}}
		{\psset{linestyle=dashed}\Tr{$S$}}
		\begin{psTree}{\psset{linestyle=dashed}\Tr{}}
		    {\psset{linestyle=dashed}\Tr{$K$}}
		    \Tr{$S$}
		\end{psTree}
	    \end{psTree}
	    \begin{psTree}{\Tcircle{1}}
		\begin{psTree}{\psset{linestyle=dashed}\Tr{}}
		    {\psset{linestyle=dashed}\Tr{$S$}}
		    \begin{psTree}{\psset{linestyle=dashed}\Tr{}}
			{\psset{linestyle=dashed}\Tr{$K$}}
			\Tr{$K$}
		    \end{psTree}
		\end{psTree}
		\Tcircle{$I$}
	    \end{psTree}
	\end{psTree}
    \end{psTree}
    \begin{psTree}{\psset{linestyle=dashed}\Tr{}}
	{\psset{linestyle=dashed}\Tr{$K$}}
	\begin{psTree}{\Tr{}}
	\begin{psTree}{\Tr{}}
	    \Tr{$S$}
	    \Tr{$I$}
	\end{psTree}
        \begin{psTree}{\Tr{}}
	    \Tr{$K$}
	    \Tr{$I$}
	\end{psTree}
	\end{psTree}
    \end{psTree}
\end{psTree}

2. Each $\lambda^*$-abstraction is removing separately of others.
\end{frame}

\begin{frame}
\frametitle{Active node}
\begin{df}
Let $x$ be a variable in a combinator $C$ and let $\lambda^*x$ be an 
abstraction removing from $C$ during the ST algorithm. A node is an 
\textbf{active} if it is on a path leading from a leaf $x$ to the root.
\end{df}
\bigskip
We show that for each $\lambda^*$-abstraction, all operations performed
on active nodes are independently to each other.
\bigskip
Let $a$ be an active node in a tree of some combinator. We show that 
for each active node we have to add at most 4 new nodes and the places 
of the new nodes in a tree are determined. We are given the following 
cases.

\end{frame}

\begin{frame}
The active node $a$ is a leaf.

\begin{figure}[H]
\centering

\begin{subfigure}[b]{0.4\textwidth}
\begin{psTree}{\Tr{}}	
\Tcircle{a}
\end{psTree}
\end{subfigure}
\hfill
\begin{subfigure}[b]{0.4\textwidth}
\begin{psTree}{\Tr{}}
\Tr{I}
\end{psTree}
\end{subfigure}

\end{figure}

Since we allow the abbreviation: $I = SKK$ then the node $a$ is changed to 
combinator $I$. There are no new nodes.\\

\end{frame}
\begin{frame}
Only the left child of $a$ is active.

\begin{figure}[H]
\centering

\begin{subfigure}[b]{0.4\textwidth}
\begin{psTree}{\Tr{}}
\begin{psTree}{\Tcircle{a}}
\Tcircle{M}
\Tr{N}
\end{psTree}
\end{psTree}
\end{subfigure}
\hfill
\begin{subfigure}[b]{0.4\textwidth}
\begin{psTree}{\Tr{}}
    \begin{psTree}{\Tr{a}}
	\begin{psTree}{\psset{linestyle=dashed}\Tr{}}
	    {\psset{linestyle=dashed}\Tr{S}}
	    \Tr{M}
	\end{psTree}
	\begin{psTree}{\psset{linestyle=dashed}\Tr{}}
	    {\psset{linestyle=dashed}\Tr{K}}
	    \Tr{N}
	\end{psTree}
    \end{psTree}
\end{psTree}
\end{subfigure}

\end{figure}

There are 4 new nodes in the tree: $S, K$ and two internal nodes.
Notice the node $a$ does not change its pointer to the father in the tree.
The node $a$ changes the pointer to the father of the active node $M$.\\
\end{frame}
\begin{frame}
Only the right child of $a$ is active.

\begin{figure}[H]
\centering

\begin{subfigure}[b]{0.4\textwidth}
\begin{psTree}{\Tr{}}
\begin{psTree}{\Tcircle{a}}
\Tr{M}
\Tcircle{N}
\end{psTree}
\end{psTree}
\end{subfigure}
\hfill
\begin{subfigure}[b]{0.4\textwidth}
\begin{psTree}{\Tr{}}
    \begin{psTree}{\Tr{a}}
	\begin{psTree}{\psset{linestyle=dashed}\Tr{}}
	    {\psset{linestyle=dashed}\Tr{S}}
	    \begin{psTree}{\psset{linestyle=dashed}\Tr{}}
		{\psset{linestyle=dashed}\Tr{K}}
		\Tr{M}
	    \end{psTree}
	\end{psTree}
	\Tr{N}
    \end{psTree}
\end{psTree}
\end{subfigure}

\end{figure}

There are 4 new nodes in the tree: $S, K$ and two internal nodes.
Notice the node $a$ does not change its pointer to the father in the tree.
The node $a$ changes the pointer to the father of the active node $N$.\\

\end{frame}
\begin{frame}
The left and the right child of $n$ are active.

\begin{figure}[H]
\centering

\begin{subfigure}{0.4\textwidth}
\begin{psTree}{\Tr{}}
\begin{psTree}{\Tcircle{a}}
\Tcircle{M}
\Tcircle{N}
\end{psTree}
\end{psTree}
\end{subfigure}
\hfill
\begin{subfigure}{0.4\textwidth}
\begin{psTree}{\Tr{}}
    \begin{psTree}{\Tr{a}}
	\begin{psTree}{\psset{linestyle=dashed}\Tr{}}
	    {\psset{linestyle=dashed}\Tr{S}}
	    \Tr{M}
	\end{psTree}
	\Tr{N}
    \end{psTree}
\end{psTree}
\end{subfigure}

\end{figure}

There are 2 new nodes in the tree: $S$ and one internal node.
Notice the node $a$ does not change its pointer to the father in the tree.
The node $a$ changes the pointer to the father of the active node $M$.
\end{frame}
\begin{frame}
Since there are no more cases, we see that for each 
$\lambda^*$-abstraction and for each active node the algorithm adds at 
most 4 new nodes to the combinator tree and the places of the new nodes
are determined and independent from new nodes added by other active nodes.\\
\bigskip
Moreover, in each case an active node do not change its pointer to the
father. This pointer can be changed only by its father. Therefore
the ST algorithm can be performe in parallel.\\
\bigskip
In our approach all $\lambda^*$-abstractions are performed in order, 
from the deepest towards the root. For each $\lambda^*$-abstraction
the active nodes are computed in parallel. Then for all active nodes 
the tree is rebuilded in parallel.
\end{frame}
\begin{frame}
\frametitle{Parallel ST Algorithm}
\begin{algorithm}[H]
\caption{The parallel standard translation algorithm}
\label{alg:STalgo}
\begin{algorithmic}[1]
\Statex
\Function{STranslation}{$x$}
    \If{$x$ is leaf}
	\State{Do nothing}
    \Else
	\If{$x$ is internal}
	    \State{STranslation(left(x))}
	    \State{STranslation(right(x))}
	\Else
	    \State{// $x$ is $\lambda*$ abstraction}
	    \State{Mark in parallel all active nodes}
	    \State{For all active nodes rebuild tree in parallel}
	\EndIf
    \EndIf
\EndFunction
\end{algorithmic}
\end{algorithm}

\end{frame}
\begin{frame}
\frametitle{Active nodes}
We use N threads assigned to N nodes in a tree. Let $\lambda^*x$ be
an abstraction reduced during the algorithm. The computition of
active nodes can be perfomed in two different ways to execute time.\\
\bigskip

The time of the first method is linear due to the size of a tree. We choose 
the threads assigned to the leaves. If the leaf is a variable $x$ then
a thread goes towards the $\lambda^*$-abstraction and marks all nodes on 
the path as active.\\
\bigskip
In the second method the execution time is constant but algorithm 
needs the additional memory of quadratic size due to the size of a tree.
Let $x$ be a node in a tree. The \textit{active} array is computed at the 
begining of the algorithm. Then the array is updated during adding new nodes.
\end{frame}
\begin{frame}
\begin{algorithm}[H]
\caption{The active array computation}
\label{alg:ComputeActive}
\begin{algorithmic}[1]
\Statex
\Function{MarkActive}{$x$}
    \If{left($x$) = null and right($x$) = null}
	\State{// $x$ is leaf}
	\Let{active[$x$]}{$\{x\}$}
    \Else
	\If{left($x$) $\ne$ null and right($x$) $\ne$ null}
	    \State{// $x$ is internal}
	    \State{MarkActive(left($x$))}
	    \State{MarkActive(right($x$))}
	    \Let{active[$x$]}{active[left($x$)] $\cup$ active[right($x$)]}
	\Else
	    \State{// $x$ is $\lambda*$ abstraction}
	    \State{MarkActive(left($x$))}
	    \Let{active[$x$]}{active[left($x$)] - $\{x\}$}
	\EndIf
    \EndIf
\EndFunction
\end{algorithmic}
\end{algorithm}
\end{frame}
\begin{frame}
\begin{algorithm}[H]
\caption{Rebuild tree}
\label{alg:ComputeActive}
\begin{algorithmic}[1]
\Statex
\Function{RebuildTree}{$x$}
    \If{$x$ is active}
	\If{left($x$) = null and right($x$) = null}
	    \Let{$x$}{$I$}
	\Else
	    \If{left($x$) is active and right($x$) is active}
		\State{Add 2 new nodes and set up active nodes}
	    \Else
		\If{left($x$) is active}
		    \State{Add 4 new nodes and set up active nodes}
		\Else
		    \State{Add 4 new nodes and set up active nodes}
		\EndIf
	    \EndIf
	\EndIf
    \EndIf
\EndFunction
\end{algorithmic}
\end{algorithm}
\end{frame}


%\begin{center}
%\begin{tabular}{l}
%If $x$ is an application then $active[x] := active[left(x)] 
%\cup active[right(x)]$.\\
%If $x$ is an abstraction then $active[x] := active[left(x)] - \{x\}$\\
%\end{tabular}
%\end{center}



\begin{thebibliography}{}
    \bibitem{1} Hendrik Pieter Barendregt, 1984.
	\textit{The Lambda Calculus, Its Syntax and Semantics} Studies in Logic and the Foundations of Mathematics, Volume 103, North-Holland. ISBN 0-444-87508-5
\end{thebibliography}

\end{document}
