#include "translation.h"
#include "termUtilis.h"
#include "cudaUtilis.h"
#include <cstdio>
#include <string>
using namespace std;

//#define debugRead(a...) fprintf(stderr, a)
#define debugRead(a...)

#define debugTerm(a...) fprintf(stderr, a)
//#define debugTerm(a...)

int main() {

    CUcontext cuContext;
    CUmodule cuModule = cudaInitialization(&cuContext);
    
    //Input
//    string term = "l1.1";
//    string term = "((l1.1) (l2.2))\0";
//    string term = "l1.l2.l3.l4.l5.l6.l7.l8.l9.l10.(1 (2 (3 (4 (5 (6 (7 (8 (9 10)))))))))";
    string term = "l1.l2.l3.l4.l5.l6.l7.l8.l9.l10.l11.l12.l13.l14.l15.l16.l17.l18.l19.l20.(1 (2 (3 (4 (5 (6 (7 (8 (9 (10 (11 (12 (13 (14 (15 (16 (17 (18 (19 20)))))))))))))))))))";
//    string term = "l1.l2.l3.l4.l5.l6.l7.l8.l9.l10.l11.l12.l13.l14.l15.l16.l17.l18.l19.l20.(1 (2 (1 (4 (1 (6 (1 (8 (1 (10 (1 (12 (1 (14 (1 (16 (1 (18 (1 20)))))))))))))))))))";
//    string term = "l1.l2.l3.l4.l5.l6.l7.l8.l9.l10.(10 (9 (8 (7 (6 (5 (4 (3 (2 1)))))))))";
//    string term = "l1.l2.l3.(1 (2 3))";
//    string term = "(2 l1.(1 1))";//???
//    string term = "l1.((l2.2) (l3.3))";
//    string term = "l1.l2.l3.(1 (1 1))";
//    string term = "l1.(1 (1 1))";
//    string term = "l1.l2.l3.l4.(1 (2 (3 4)))";
//    string term = "l1.l2.l3.l4.l5.(1 (2 (3 (4 5))))";
//    string term = "l1.l2.(1 2)";
    debugTerm("[%s:%d] term %s\n", __FILE__, __LINE__, term.c_str());

    //Computing the lambda term size
    int id = 0;
    int real_size = lambda_term_size(term.c_str(), &id, 0);
    int size = 10*real_size*real_size*real_size;
    debugRead("[%s:%d] size %d, %d\n", __FILE__, __LINE__, real_size, size);

    //Host memory allocation
    int *active_nodes, *up, *node, *left, *right;
    allocateHostMemory(active_nodes, up, node, left, right, size);
    
    //Parse the lambda term into arrays
    id = 0;
    int root = parser(term.c_str(), &id, -1, -1, node, up, left, right);

    //Debug term
    read_lambda_term(root, node, up, left, right);

    //Compute the leaves
    int *leaves, number_of_leaves=0;
    get_leaves_and_internals(real_size, left, right, leaves, &number_of_leaves);

    //Device memory allocation
    CUdeviceptr d_leaves, d_active_nodes, d_up, d_node, d_left, d_right;
    allocateDeviceMemory(&d_leaves, leaves, &d_active_nodes, active_nodes, &d_up, up, 
            &d_node, node, &d_left, left, &d_right, right, size, number_of_leaves);

   
    //Translate
    if (translate(size, &real_size, root, number_of_leaves, d_up, up, left, right, 
                active_nodes, d_node, node, d_leaves, d_active_nodes, d_left, d_right, 
                cuModule) > 0){
        printf("[%s:%d] Za malo pamieci\n", __FILE__, __LINE__);
        //Cleaning
        cleaning(&d_leaves, leaves, &d_active_nodes, active_nodes, &d_up, up, \
            &d_node, node, &d_left, left, &d_right, right, &cuContext);
        exit(1);
    }

    //Print result
    copyBackToHost(d_up, up, d_node, node, d_left, left, d_right, right, real_size);
    wypisz_term(root, node, up, left, right);   printf("\n");

    //Cleaning
    cleaning(&d_leaves, leaves, &d_active_nodes, active_nodes, &d_up, up, \
            &d_node, node, &d_left, left, &d_right, right, &cuContext);

    debug_totalMemoryUse(size, number_of_leaves, real_size);
    return 0;
}

