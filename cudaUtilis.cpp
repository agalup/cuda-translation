#include "cudaUtilis.h"
#include "cuda_error.h"
#include <cstdio>
#include <string>
using namespace std;

#define debugError(a...) fprintf(stderr, a)
//#define debugError(a...)

CUmodule cudaInitialization(CUcontext* cuContext){
    cuInit(0);

    CUresult res;
    CUdevice cuDevice;
    res = cuDeviceGet(&cuDevice, 0);
    if (res != CUDA_SUCCESS) debugError("err %s %d\n", CUerror(res), __LINE__);

    res = cuCtxCreate(cuContext, 0, cuDevice);
    if (res != CUDA_SUCCESS) debugError("err %s %d\n", CUerror(res), __LINE__);

    CUmodule cuModule;
    res = cuModuleLoad(&cuModule, "translation.ptx");
    if (res != CUDA_SUCCESS) debugError("err %s %d\n", CUerror(res), __LINE__);

    return cuModule;
}
void allocateHostMemory(int*& active_nodes, int*& up, int*& node, int*& left, 
        int*& right, int size){
    active_nodes = (int*)malloc(size*sizeof(int));
    up = (int*)malloc(size*sizeof(int));
    node = (int*)malloc(size*sizeof(int));
    left = (int*)malloc(size*sizeof(int));
    right = (int*)malloc(size*sizeof(int));
    for (int j=0; j<size; ++j){
        up[j] = right[j] = left[j] = node[j] = -1;
    }
}
void allocateDeviceMemory(  CUdeviceptr* d_leaves, int* leaves, 
                            CUdeviceptr* d_active_nodes, int* active_nodes,
                            CUdeviceptr* d_up, int* up,
                            CUdeviceptr* d_node, int* node,
                            CUdeviceptr* d_left, int* left,
                            CUdeviceptr* d_right, int* right, 
                            int size, int number_of_leaves){
   
    //LEAVES
    CUresult res = cuMemAlloc(d_leaves, number_of_leaves*sizeof(int));
    if (res != CUDA_SUCCESS) debugError("err %s %d\n", CUerror(res), __LINE__);
    
    res = cuMemHostRegister(leaves, number_of_leaves*sizeof(int), 0);
    if (res != CUDA_SUCCESS)
        debugError("err %s %d\n", CUerror(res), __LINE__);

    res = cuMemcpyHtoD(*d_leaves, leaves, number_of_leaves*sizeof(int));
    if (res != CUDA_SUCCESS)
        debugError("err %s  %d\n", CUerror(res) ,__LINE__);

    //ACTIVE
    res = cuMemAlloc(d_active_nodes, size*sizeof(int));
    if (res != CUDA_SUCCESS)
        debugError("err %s  %d\n", CUerror(res) ,__LINE__);
    
    res = cuMemHostRegister(active_nodes, size*sizeof(int), 0);
    if (res != CUDA_SUCCESS)
        debugError("err %s  %d\n", CUerror(res) ,__LINE__);

    //UP
    res = cuMemAlloc(d_up, size*sizeof(int));
    if (res != CUDA_SUCCESS)
        debugError("err %s  %d\n", CUerror(res) ,__LINE__);

    res = cuMemHostRegister(up, size*sizeof(int), 0);
    if (res != CUDA_SUCCESS)
        debugError("err %s  %d\n", CUerror(res) ,__LINE__);

    res = cuMemcpyHtoD(*d_up, up, size*sizeof(int));
    if (res != CUDA_SUCCESS)
        debugError("err %s  %d\n", CUerror(res) ,__LINE__);

    //NODE
    res = cuMemAlloc(d_node, size*sizeof(int));
    if (res != CUDA_SUCCESS)
        debugError("err %s  %d\n", CUerror(res) ,__LINE__);

    res = cuMemHostRegister(node, size*sizeof(int), 0);
    if (res != CUDA_SUCCESS)
        debugError("err %s  %d\n", CUerror(res) ,__LINE__);

    res = cuMemcpyHtoD(*d_node, node, size*sizeof(int));
    if (res != CUDA_SUCCESS)
        debugError("err %s  %d\n", CUerror(res) ,__LINE__);
    
    //LEFT
    res = cuMemAlloc(d_left, size*sizeof(int));
    if (res != CUDA_SUCCESS)
        debugError("err %s  %d\n", CUerror(res) ,__LINE__);
    
    res = cuMemHostRegister(left, size*sizeof(int), 0);
    if (res != CUDA_SUCCESS)
        debugError("err %s %d\n", CUerror(res), __LINE__);
    
    res = cuMemcpyHtoD(*d_left, left, size*sizeof(int));     
    if (res != CUDA_SUCCESS)
        debugError("err %s  %d\n", CUerror(res) ,__LINE__);

    //RIGHT
    res = cuMemAlloc(d_right, size*sizeof(int));
    if (res != CUDA_SUCCESS)
        debugError("err %s  %d\n", CUerror(res) ,__LINE__);
    
    res = cuMemHostRegister(right, size*sizeof(int), 0);
    if (res != CUDA_SUCCESS)
        debugError("err %s %d\n", CUerror(res), __LINE__);
   
    res = cuMemcpyHtoD(*d_right, right, size*sizeof(int));
    if (res != CUDA_SUCCESS)
        debugError("err %s  %d\n", CUerror(res) ,__LINE__);
   
    //Sync
    res = cuCtxSynchronize();
    if (res != CUDA_SUCCESS) debugError("err %s %d\n", CUerror(res), __LINE__);
}
void debug_totalMemoryUse(int size, int numberOfLeaves, int real_size){
    double wasMemory = (5*(double)size + (double)numberOfLeaves)*4;
    string g;
//    char* g = new char[3];
    if (wasMemory > 1000*1000*1000){
        wasMemory /= 1000*1000*1000;
        g = "GB\0";
    }else if (wasMemory > 1000*1000){
        wasMemory /= 1000*1000;
        g = "MB\0";
    }else if (wasMemory > 1000){
        wasMemory /= 1000;
        g = "KB\0";
    }else{
        g = "B\0";
    }
    printf("Device/Host memory use: %lf%s\n", wasMemory, g.c_str());
    printf("(5*%d + %d)*2*4B\n", size, numberOfLeaves);
    double couldBeMemory = ((double)numberOfLeaves + 5*(double)real_size)*4;
    if (couldBeMemory > 1000*1000*1000){
        couldBeMemory /= 1000*1000*1000;
        g = "GB\0";
    }else if (couldBeMemory > 1000*1000){
        couldBeMemory /= 1000*1000;
        g = "MB\0";
    }else if (couldBeMemory > 1000){
        couldBeMemory /= 1000;
        g = "KB\0";
    }else{
        g = "B\0";
    }
    printf("Device/Host memory could be use: %lf%s\n", couldBeMemory, g.c_str());
    printf("(5*%d + %d)*2*4B\n", real_size, numberOfLeaves);
}

void copyBackToHost(CUdeviceptr d_up, int* up, CUdeviceptr d_node, int* node, 
        CUdeviceptr d_left, int* left, CUdeviceptr d_right, int* right, int real_size){
    
    CUresult res = cuMemcpyDtoH(up, d_up, real_size*sizeof(int));
    if (res != CUDA_SUCCESS)
        debugError("err %s  %d\n", CUerror(res) ,__LINE__);

    res = cuMemcpyDtoH(node, d_node, real_size*sizeof(int));
    if (res != CUDA_SUCCESS)
        debugError("err %s  %d\n", CUerror(res) ,__LINE__);
    
    res = cuMemcpyDtoH(left, d_left, real_size*sizeof(int));     
    if (res != CUDA_SUCCESS)
        debugError("err %s  %d\n", CUerror(res) ,__LINE__);
    
    res = cuMemcpyDtoH(right, d_right, real_size*sizeof(int));
    if (res != CUDA_SUCCESS)
        debugError("err %s  %d\n", CUerror(res) ,__LINE__);
    
    res = cuCtxSynchronize();
    if (res != CUDA_SUCCESS) debugError("err %s %d\n", CUerror(res), __LINE__);
}
void cleaning(  CUdeviceptr* d_leaves, int* leaves, 
                CUdeviceptr* d_active_nodes, int* active_nodes,
                CUdeviceptr* d_up, int* up,
                CUdeviceptr* d_node, int* node,
                CUdeviceptr* d_left, int* left,
                CUdeviceptr* d_right, int* right,
                CUcontext* cuContext){
    free(leaves);
    free(active_nodes);
    free(up);
    free(node);
    free(left);
    free(right);
    CUresult res = cuMemFree(*d_leaves);
    if (res != CUDA_SUCCESS) debugError("err %s  %d\n", CUerror(res) ,__LINE__);
    res = cuMemFree(*d_active_nodes);
    if (res != CUDA_SUCCESS) debugError("err %s  %d\n", CUerror(res) ,__LINE__);
    res = cuMemFree(*d_up);
    if (res != CUDA_SUCCESS) debugError("err %s  %d\n", CUerror(res) ,__LINE__);
    res = cuMemFree(*d_node);
    if (res != CUDA_SUCCESS) debugError("err %s  %d\n", CUerror(res) ,__LINE__);
    res = cuMemFree(*d_left);
    if (res != CUDA_SUCCESS) debugError("err %s  %d\n", CUerror(res) ,__LINE__);
    res = cuMemFree(*d_right);
    if (res != CUDA_SUCCESS) debugError("err %s  %d\n", CUerror(res) ,__LINE__);
    
    cuCtxDestroy(*cuContext);
}
