#ifndef CUDA_ERROR
#define CUDA_ERROR
#include "cuda.h"

using namespace std;

const char *CUerror(CUresult result);

#endif 
