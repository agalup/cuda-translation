#include "translation.h"
#include "termUtilis.h"
#include "cudaUtilis.h"
#include "cuda_error.h"
#include <cstdio>

#define debugError(a...)    fprintf(stderr, a)
//#define debugError(a...)

//#define debugRead(a...)    fprintf(stderr, a)
#define debugRead(a...)

#define debug(a...)    fprintf(stderr, a)
//#define debug(a...)

int countNewNodes(int last, CUdeviceptr d_active_nodes, int* active_nodes, CUdeviceptr d_left, 
        CUdeviceptr d_right, CUmodule cuModule){
    int gridDim = (last+1023)/1024;
    int blockDim = 1024;

    CUfunction countNewNodes;
    CUresult res = cuModuleGetFunction(&countNewNodes, cuModule, "countNewNodes");
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);

    void* args_countNewNodes[] = {&last, &d_left, &d_right, &d_active_nodes};
    res = cuLaunchKernel(countNewNodes, gridDim, 1, 1, blockDim, 1, 1, 0, 0, args_countNewNodes, 0);
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);

    res = cuCtxSynchronize();
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);

    CUdeviceptr d_last_in_block;
    res = cuMemAlloc(&d_last_in_block, gridDim * sizeof(int));
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);
    CUfunction prefixSum;
    res = cuModuleGetFunction(&prefixSum, cuModule, "prefixSum");
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);
    void* args_prefixSum[] = {&last, &d_active_nodes, &d_last_in_block};
    res = cuLaunchKernel(prefixSum, gridDim, 1, 1, blockDim, 1, 1, 0, 0, args_prefixSum, 0);
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);
    res = cuCtxSynchronize();
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);

    int* last_in_block = (int*)malloc(gridDim * sizeof(int));
    res = cuCtxSynchronize();
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);
    res = cuMemHostRegister(last_in_block, gridDim * sizeof(int), 0);
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);

    res = cuMemcpyDtoH(last_in_block, d_last_in_block, gridDim * sizeof(int));
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);
    res = cuCtxSynchronize();
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);

    for (int i=1; i<gridDim; ++i){
        last_in_block[i] += last_in_block[i-1];
    }

    res = cuMemcpyHtoD(d_last_in_block, last_in_block, gridDim * sizeof(int));
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);
    res = cuCtxSynchronize();
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);

    CUdeviceptr d_last;
    res = cuMemAlloc(&d_last, sizeof(int));
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);

    CUfunction prefixSumEnd;
    res = cuModuleGetFunction(&prefixSumEnd, cuModule, "prefixSumEnd");
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);
    void* args_prefixSumEnd[] = {&last, &d_active_nodes, &d_last_in_block, &d_last};
    res = cuLaunchKernel(prefixSumEnd, gridDim, 1, 1, blockDim, 1, 1, 0, 0, args_prefixSumEnd, 0);
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);
    res = cuCtxSynchronize();
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);

    int number_new_nodes = 0;
    res = cuMemcpyDtoH(&number_new_nodes, d_last, sizeof(int));
    if (res != CUDA_SUCCESS)
        debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);
 
    res = cuMemcpyDtoH(active_nodes, d_active_nodes, last*sizeof(int));
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);
    
    //debug
    debugRead("active nodes:\n");
    for (int j=0; j<last; ++j){
        debugRead("%d ", active_nodes[j]);
    }
    debugRead("\n");

   
    return number_new_nodes;
}
bool markActive(CUdeviceptr d_active_nodes, int* active_nodes, CUdeviceptr d_up, 
        CUdeviceptr d_node, CUdeviceptr d_leaves, CUdeviceptr d_left, CUdeviceptr d_right,
        CUmodule cuModule, int last, int root, int number_of_leaves){ 

    CUresult res = cuMemsetD32(d_active_nodes, 0, last);
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);
    
    CUfunction setActive;
    res = cuModuleGetFunction(&setActive, cuModule, "setActive");
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);
 
    int gridDim = (number_of_leaves+1023)/1024;
    int blockDim = 1024;
    void* args_setActive[] = {&last, &root, &number_of_leaves, &d_up, &d_node, 
        &d_leaves, &d_active_nodes, &d_left, &d_right};
    res = cuLaunchKernel(setActive, gridDim, 1, 1, blockDim, 1, 1, 0, 0, args_setActive, 0);
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);

    res = cuCtxSynchronize();
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);

    res = cuMemcpyDtoH(active_nodes, d_active_nodes, last*sizeof(int));
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);
    
    //debug
    debugRead("active nodes:\n");
    for (int j=0; j<last; ++j){
        debugRead("%d ", active_nodes[j]);
    }
    debugRead("\n");

    return true;
}
bool markActive_byPrefix(CUdeviceptr d_active_nodes, int* active_nodes, CUdeviceptr d_up, int* up, 
        CUdeviceptr d_node, CUdeviceptr d_leaves, CUdeviceptr d_left, CUdeviceptr d_right, 
        CUmodule cuModule, int last, int root, int number_of_leaves){ 

    //All nodes are inactive
    CUresult res = cuMemsetD32(d_active_nodes, 0, last);
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);

    int gridDim = (last+1023)/1024;
    int blockDim = 1024;
    CUdeviceptr d_isActiveLeaf;
    res = cuMemAlloc(&d_isActiveLeaf, sizeof(int));
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);
    res = cuMemsetD32(d_isActiveLeaf, 0, 1);
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);

    //Mark active leaves and drop out lambda abstraction
    CUfunction setActiveLeaves;
    res = cuModuleGetFunction(&setActiveLeaves, cuModule, "setActiveLeaves");
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);
    void* args_setActiveLeaves[] = {&last, &root, &d_up, &d_left, &d_right, &d_node, &d_active_nodes, &d_isActiveLeaf};
    res = cuLaunchKernel(setActiveLeaves, gridDim, 1, 1, blockDim, 1, 1, 0, 0, args_setActiveLeaves, 0);
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);

    //Copy d_up array
    CUdeviceptr d_up2; 
    res = cuMemAlloc(&d_up2, sizeof(int)*last);
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);
    res = cuMemcpyDtoD(d_up2, d_up, sizeof(int)*last);
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);
    res = cuMemcpyDtoH(up, d_up, sizeof(int)*last);
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);

    //debug
/*    res = cuMemcpyDtoH(active_nodes, d_active_nodes, last*sizeof(int));
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);
    debugRead("active leaves:\n");
    for (int j=0; j<last; ++j){
        debugRead("%d ", active_nodes[j]);
    }
    debugRead("\n");*/

    //Mark active nodes
    CUfunction setUp;
    res = cuModuleGetFunction(&setUp, cuModule, "setUp");
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);
    CUdeviceptr d_flag;
    res = cuMemAlloc(&d_flag, sizeof(int));
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);
    void* args_setUp[] = {&last, &root, &d_up, &d_up2, &d_active_nodes, &d_flag};
    void* args_setUp2[] = {&last, &root, &d_up2, &d_up, &d_active_nodes, &d_flag};
    int iter = 0;
    while (1){
//    for (int i=1; i<last*last; i<<=2){
        res = cuMemsetD32(d_flag, 0, 1);
        if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);
       
        if (iter%2 == 0){
            res = cuLaunchKernel(setUp, gridDim, 1, 1, blockDim, 1, 1, 0, 0, args_setUp, 0);
            if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);
            res = cuCtxSynchronize();
            if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);
        }else{
            res = cuLaunchKernel(setUp, gridDim, 1, 1, blockDim, 1, 1, 0, 0, args_setUp2, 0);
            if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);
            res = cuCtxSynchronize();
            if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);
        }
        //debug
/*        res = cuMemcpyDtoH(active_nodes, d_active_nodes, last*sizeof(int));
        if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);
        debugRead("active nodes:\n");
        for (int j=0; j<last; ++j){
            debugRead("%d ", active_nodes[j]);
        }
        debugRead("\n");
*/
        int flag = 0;
        res = cuMemcpyDtoH(&flag, d_flag, sizeof(int));
        if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);
        res = cuCtxSynchronize();
        if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);
        if (flag == 0)
            break;
        ++iter;
    }

    bool freeVariable = true;
    if (iter == 0){//There is no variable = node[root]
        int isActiveLeaf = 0;
        res = cuMemcpyDtoH(&isActiveLeaf, d_isActiveLeaf, sizeof(int));
        if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);
        if (isActiveLeaf == 0){
            debug("There is no variable of root id %d\n", root);
            freeVariable = false;
        }
    }else{
        //Restor the up array
        res = cuMemcpyHtoD(d_up, up, sizeof(int)*last);
        if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);
    }

    //Cleaning
    res = cuMemFree(d_up2); 
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);
    res = cuMemFree(d_flag); 
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);
    res = cuMemFree(d_isActiveLeaf); 
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);

    //debug
   /* res = cuMemcpyDtoH(active_nodes, d_active_nodes, last*sizeof(int));
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);
    debug("active nodes:\n");
    for (int j=0; j<last; ++j){
        debug("%d ", active_nodes[j]);
    }
    debug("\n");*/

    return freeVariable;
}
void addNewNodes(int last, CUdeviceptr d_active_nodes, CUdeviceptr d_node, CUdeviceptr d_up, 
        CUdeviceptr d_left, CUdeviceptr d_right, CUmodule cuModule){

    CUfunction translate;
    CUresult res = cuModuleGetFunction(&translate, cuModule, "translate");
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);
   
    int gridDim = (last+1023)/1024;
    int blockDim = 1024;
    void* args_translate[] = {&last, &d_active_nodes, &d_node, &d_up, &d_left, &d_right}; 
    res = cuLaunchKernel(translate, gridDim, 1, 1, blockDim, 1, 1, 0, 0, args_translate, 0);
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);
    res = cuCtxSynchronize();
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);
}
void addKCombinator(bool freeVariable, int last, int root, CUdeviceptr d_node, CUdeviceptr d_up, CUdeviceptr d_left, 
        CUdeviceptr d_right, CUmodule cuModule){
//    int gridDim = (last+1023)/1024;
//    int blockDim = 1024;
    void* args_addKCombinator[] = {&freeVariable, &last, &root, &d_node, &d_up, &d_left, &d_right};
    CUfunction addKCombinator;
    CUresult res = cuModuleGetFunction(&addKCombinator, cuModule, "addKCombinator");
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);
    res = cuLaunchKernel(addKCombinator, 1, 1, 1, 1, 1, 1, 0, 0, args_addKCombinator, 0);
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);
    res = cuCtxSynchronize();
    if (res != CUDA_SUCCESS) debugError("[%s] err %s  %d\n", __FILE__, CUerror(res), __LINE__);
}
int translate(int N, int* real_size, int root, int number_of_leaves, CUdeviceptr d_up, int* up, 
        int* left, int* right, int* active_nodes, CUdeviceptr d_node, int* node, CUdeviceptr d_leaves, 
        CUdeviceptr d_active_nodes, CUdeviceptr d_left, CUdeviceptr d_right, CUmodule cuModule){
    //Leaf
    if (left[root] == -1){
        return 0;
    }

    //Internal
    if (right[root] != -1){
        translate(N, real_size, left[root], number_of_leaves, d_up, up, left, right, active_nodes, 
                d_node, node, d_leaves, d_active_nodes, d_left, d_right, cuModule);
        translate(N, real_size, right[root], number_of_leaves, d_up, up, left, right, active_nodes, 
                d_node, node, d_leaves, d_active_nodes, d_left, d_right, cuModule);
        return 0;
    }

    //Lambda abstraction
    translate(N, real_size, left[root], number_of_leaves, d_up, up, left, right, active_nodes, 
            d_node, node, d_leaves, d_active_nodes, d_left, d_right, cuModule);

    //Mark active nodes
    bool freeVariable = markActive_byPrefix(d_active_nodes, active_nodes, d_up, up, d_node, d_leaves, d_left, d_right, \
            cuModule, *real_size, root, number_of_leaves);
//    bool freeVariable =  markActive(d_active_nodes, active_nodes, d_up, d_node, d_leaves, d_left, d_right, cuModule, \
    *real_size, root, number_of_leaves);

    int new_nodes = 0;
    
    //Add K combinator or drop out lambda abstraction 
    debug("Add K combinator or drop out lambda abstraction\n");
    addKCombinator(freeVariable, *real_size, root, d_node, d_up, d_left, d_right, cuModule);

    if (freeVariable){
        //Compute the number of new nodes
        new_nodes = countNewNodes(*real_size, d_active_nodes, active_nodes, d_left, d_right, cuModule);
    
        //Create the new nodes
        if ((*real_size)+new_nodes > N){
            return N+new_nodes;
        }
        
        //Translation
        addNewNodes(*real_size, d_active_nodes, d_node, d_up, d_left, d_right, cuModule);
    }else{
        new_nodes = 1;
    }

    debug("new nodes %d\n", new_nodes);
  
    *real_size += new_nodes;
 
    //Print result
    //copyBackToHost(d_up, up, d_node, node, d_left, left, d_right, right, *real_size);
    //wypisz_term(root, node, up, left, right); printf("\n");

    return 0;
}
