#ifndef TRANSLATION
#define TRANSLATION

#include <cstdio>
#include <climits>

//#define debug(a...) printf(a)
#define debug(a...) 

extern "C"
__global__
void setActiveLeaves(int N, int root, int* up, int* left, int* right, int* node, int* active_nodes, int* isActiveLeaf){
    int thid = blockIdx.x*1024+threadIdx.x;
    if (thid < N && thid != root && node[thid] == node[root]){
        active_nodes[thid] = 1;
        *isActiveLeaf = 1;
    }
}
extern "C"
__global__
void addKCombinator(bool flag, int last, int root, int* node, int* up, int* left, int* right){
    if (flag){
        int up_root = up[root];
        if (up_root != -1){
            if (left[up_root] == root)
                left[up_root] = left[root];
            else 
                right[up_root] = left[root];
        }
    }else{
        node[root] = 0;
        right[root] = left[root];
        left[root] = last;
        node[last] = -2;
        left[last] = -1;
        right[last] = -1;
        up[last] = root;
    }
}
extern "C"
__global__
void setUp(int N, int root, int* up, int* up2, int* active_nodes, int* flag){
    int thid = blockIdx.x*1024 + threadIdx.x;
    if (thid >= N || thid == root)
        return;
    int up_thid = up[thid];
    if (up_thid > -1 && up_thid != root){
        if (active_nodes[thid] == 1 && active_nodes[up_thid] == 0){
            active_nodes[up_thid] = 1;
            *flag = 1;
        }
        up2[thid] = up[up_thid];
    }
}
extern "C"
__global__
void setActive(int N, int root, int number_of_leaves, int* up, int* node, int* leaves, int* active, 
        int* left, int* right){
    int thid = blockIdx.x*1024 + threadIdx.x;
    if (thid == 0)
        debug("number_of_leaves=%d\n", number_of_leaves);
    if (thid < number_of_leaves){//the number of threads at most the number of leaves
        int leaf = leaves[thid];
        int var = node[root];
        if (node[leaf] == var){
            debug("leaf = %d\n", node[leaf]);
            active[leaf] = 1;
            int id = up[leaf];
            while (node[id] != var){
                debug("mark as active id %d\n", id);
                if (active[id] > 0) break;
                active[id] = 1;
                id = up[id];
            }
            //id is lambda
            if (up[id] != -1){
                if (left[up[id]] == id)
                    left[up[id]] = left[id];
                else if (right[up[id]] == id)
                    right[up[id]] = left[id];
            }
            up[left[id]] = up[id];
            if (up[id] != -1)
                debug("up[%d] %d, node %d, up %d, left %d, right %d, up[left[id]] %d\n", id, up[id], 
                        node[up[id]], up[up[id]], left[up[id]], right[up[id]], up[left[id]]);
        }
    }
}
extern "C"
__global__
void countNewNodes(int N, int* left, int*right, int* active){
    int thid = blockIdx.x*1024 + threadIdx.x;
    //the number of threads at most the number of nodes
    if (thid < N && active[thid] > 0){
        int idl = left[thid];
        int idr = right[thid];
        int l = 0, r = 0;
        if (idl >= 0)
            l = active[idl];
        if (idr >= 0)
            r = active[idr];
        if (idr == -1 && idl == -1)
            active[thid] = 4;
        else if (l > 0 && r > 0)
            active[thid] = 2;
        else if (l >= 0 || r >= 0)
            active[thid] = 4;
        debug("[%s, %d], active id = %d, left %d, right %d, new nodes: %d\n", 
                __FILE__, __LINE__, thid, idl, idr, active[thid]);
    }
}
extern "C"
__global__
void prefixSum(int N, int* active, int* last_in_block){
    if (threadIdx.x == 0)
        debug("nr bloku %d\n", blockIdx.x);
    int thid = blockIdx.x*1024 + threadIdx.x;
    __shared__ int tab[1024];
    tab[threadIdx.x] = (thid < N ? active[thid] : 0);
    __syncthreads();
    //if (thid < N) debug("[%s, %d], tab[%d] = %d\n", __FILE__, __LINE__, threadIdx.x, tab[threadIdx.x]);
    for (int i=1; i<1024; i*=2){
        int tmp = 0;
        if (threadIdx.x >= i){
            tmp = tab[threadIdx.x - i];
        }
        __syncthreads();
        if (threadIdx.x >= i){
            tab[threadIdx.x] += tmp;
        }
        __syncthreads();
    }
    __syncthreads();
    //if (thid < N) debug("[%s, %d], tab[%d] = %d\n", __FILE__, __LINE__, threadIdx.x, tab[threadIdx.x]);
    if (thid < N){
  //      debug("%d: tab %d\n", thid, tab[threadIdx.x]);
        active[thid] = tab[threadIdx.x];
    }
    if (threadIdx.x == 1023){
        last_in_block[blockIdx.x] = tab[threadIdx.x];
    }
}
extern "C"
__global__
void prefixSumEnd(int N, int* active, int* last_in_block, int* last){
    int thid = blockIdx.x*1024 + threadIdx.x;
    if (thid < N){
        if (blockIdx.x == 0){
//            debug("do active %d dodaje %d\n", active[thid], N);
            active[thid] += N;
        }else{
            active[thid] += (last_in_block[blockIdx.x-1] + N);
        }
        if (thid == N-1){
            *last = active[thid] - N;
        }
    }
}
extern "C"
__global__
void translate(int N, int* active, int* node, int* up, int* left, int* right){
    int thid = blockIdx.x*1024 + threadIdx.x;
    if (thid == 0)
        debug("N=%d\n", N);
    if (thid >= N)
        return;
    int a = active[thid];
    if ((thid == 0 && a > N) || (thid > 0 && thid < N && a != active[thid-1])){
        debug("[%s:%d] active node %d, a = %d\n", __FILE__, __LINE__, thid, a);
        a = (thid == 0 ? N : active[thid-1]);
        int l = left[thid];
        int r = right[thid];
        if (l == -1 && r == -1){
            debug("[%s:%d] add SKK\n", __FILE__, __LINE__);
//            debug("node %d jest zmienna\n", thid);
            node[thid] = 0;
            left[thid] = a;
            left[a] = a+1;
            right[a] = a+2;
            right[thid] = a+3;
            up[a] = thid;
            up[a+1] = a;
            up[a+2] = a;
            up[a+3] = thid;
            node[a] = 0;
            node[a+1] = -3; //S
            node[a+2] = -2; //K
            node[a+3] = -2; //K
            left[a+1] = right[a+1] = left[a+2] = right[a+2] = left[a+3] = right[a+3] = -1;
            debug("%d node %d up %d left %d right %d\n", thid, node[thid], up[thid], left[thid], right[thid]);
            for (int i=a; i<a+4; ++i){
                debug("%d node %d: up %d, left %d, right %d\n", i, node[i], up[i], left[i], right[i]);
            }
        }else{
            bool left_is_active = (l == 0 && active[l] > N ? true : (l > 0 && active[l] != active[l-1] ? true : false));
            bool right_is_active = (r == 0 && active[r] > N ? true : (r > 0 && active[r] != active[r-1] ? true : false));
            if (left_is_active && right_is_active){
                debug("[%s:%d] add 2 new nodes\n", __FILE__, __LINE__);
                node[thid] = 0;
                left[thid] = a;
                left[a] = a+1;
                right[a] = l; //old left[thid];
                up[a] = thid;
                up[a+1] = a;
                up[l] = a;
                node[a] = 0;
                node[a+1] = -3; //S
                debug("%d node %d up %d left %d right %d\n", thid, node[thid], up[thid], left[thid], right[thid]);
                for (int i=a; i<a+2; ++i){
                    debug("%d node %d: up %d, left %d, right %d\n", i, node[i], up[i], left[i], right[i]);
                }

            }else if (left_is_active){
                debug("[%s:%d] add 4 new nodes\n", __FILE__, __LINE__);
                left[thid] = a;
                left[a] = a+1;
                right[a] = l;
                up[l] = a;
                right[thid] = a+2;
                left[a+2] = a+3;
                right[a+2] = r;
                up[r] = a+2;
                up[a] = thid;
                up[a+1] = a;
                up[l] = a;
                up[a+2] = thid;
                up[a+3] = a+2;
                node[a] = 0;
                node[a+1] = -3; //S
                node[a+2] = 0;
                node[a+3] = -2; //K
                debug("%d node %d up %d left %d right %d\n", thid, node[thid], up[thid], left[thid], right[thid]);
                for (int i=a; i<a+4; ++i){
                    debug("%d node %d: up %d, left %d, right %d\n", i, node[i], up[i], left[i], right[i]);
                }

            }else{ //right_is_active
                debug("[%s:%d] add 4 new nodes\n", __FILE__, __LINE__);
                left[thid] = a;
                left[a] = a+1;
                right[a] = a+2;
                left[a+2] = a+3;
                right[a+2] = l;
                up[l] = a+2;
                up[a] = thid;
                up[a+1] = a;
                up[a+2] = a;
                up[a+3] = a+2;
                node[a] = 0;
                node[a+1] = -3; //S
                node[a+2] = 0;
                node[a+3] = -2; //K
                debug("%d node %d up %d left %d right %d\n", thid, node[thid], up[thid], left[thid], right[thid]);
                for (int i=a; i<a+4; ++i){
                    debug("%d node %d: up %d, left %d, right %d\n", i, node[i], up[i], left[i], right[i]);
                }

            }
        }
    }
}
#endif

