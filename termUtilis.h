
int variable_size(const char* Term, int* i, int term_size);
void dot_size(int* i);
int lambda_term_size(const char* Term, int* i, int term_size);

int variable_parse(const char* Term, int* i);
void dot_parse(int* i);
int parser(const char* Term, int* i, int term_size, int ojciec, int* node, int* up, int* left, int* right);

void get_leaves_and_internals(int size, int* left, int* right, int*& leaves, 
        int *number_of_leaves);

void read_lambda_term(int root, int* node, int* up, int* left, int* right);

void wypisz_term(int root, int* node, int* up, int* left, int* right);

