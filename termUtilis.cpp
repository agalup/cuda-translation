#include "termUtilis.h"
#include <cstdio>
#include <algorithm>
#include <vector>
using namespace std;

//#define debugSize(a...) fprintf(stderr, a)
#define debugSize(a...)

//#define debugRead(a...) fprintf(stderr, a)
#define debugRead(a...)

int variable_size(const char* Term, int* i, int term_size){
    if (Term[*i] == '\0'){
        debugSize("parsing error %d\n", __LINE__);
        exit(-1);
    }
    while (Term[*i] != ' ' && Term[*i] != '.' && Term[*i] != '(' && Term[*i] != ')'){
        ++(*i);
    }
    return term_size + 1;
}
void dot_size(int* i){
    ++(*i);
    return;
}
int lambda_term_size(const char* Term, int* i, int term_size){
    debugSize("[%d]%c\n", *i, Term[*i]);
    while (Term[*i] == ' ') {
        ++(*i);
    }
    switch (Term[*i]){
        case 'l':
            // lambda
            ++(*i);
            term_size = variable_size(Term, i, term_size);
            dot_size(i);
            term_size = lambda_term_size(Term, i, term_size);
            break;
        case '(': 
            {
                // application or lambda abstraction
                ++(*i);
                term_size = lambda_term_size(Term, i, term_size);
                // check whether it's an application
                if (Term[*i] != ')'){
                    term_size = 1 + lambda_term_size(Term, i, term_size);
                }
                if (Term[*i] != ')') {
                    debugSize("parsing error %d\n", __LINE__);
                    exit(-1);
                }
                ++(*i);
            }
            break;
        case '\0':
            break;
        default:
            // variable
            term_size = variable_size(Term, i, term_size);
    }
    return term_size;
}
int variable_parse(const char* Term, int* i){
    if (Term[*i] == '\0'){
        debugSize("parsing error %d\n", __LINE__);
        exit(-1);
    }
    int id = 0;
    char buffer[256];
    while (Term[*i] != ' ' && Term[*i] != '.' && Term[*i] != '(' && Term[*i] != ')'){
        buffer[id] = Term[*i];
        ++id;
        ++(*i);
    }
    buffer[id] = '\0';
    return atoi(buffer);;
}
void dot_parse(int* i){
    ++(*i);
    return;
}
int parser(const char* Term, int* i, int term_id, int ojciec, int* node, int* up, int* left, int* right){
    debugSize("[%d]%c\n", *i, Term[*i]);
    while (Term[*i] == ' ') {
        ++(*i);
    }
    int child_term_id = -1, right_child = -1, left_child = -1, var;
    switch (Term[*i]){
        case 'l':
            // lambda
            ++(*i);
            var = variable_parse(Term, i);
            dot_parse(i);
            child_term_id = parser(Term, i, term_id, term_id, node, up, left, right);
            term_id = child_term_id + 1;
            node[term_id] = var;
            up[term_id] = ojciec;
            left[term_id] = child_term_id;
            up[child_term_id] = term_id;
            break;
        case '(': 
            {
                // application or lambda abstraction
                ++(*i);
                term_id = parser(Term, i, term_id, term_id, node, up, left, right);
                // check whether it's an application
                if (Term[*i] != ')'){
                    left_child = term_id;
                    right_child = parser(Term, i, term_id, term_id, node, up, left, right);
                    term_id = right_child + 1;
                    node[term_id] = 0;
                    right[term_id] = right_child;
                    left[term_id] = left_child;
                    up[right_child] = term_id;
                    up[left_child] = term_id;
                }
                up[term_id] = ojciec;
                if (Term[*i] != ')') {
                    debugSize("parsing error %d\n", __LINE__);
                    exit(-1);
                }
                ++(*i);
            }
            break;
        case '\0':
            break;
        default:
            // variable
            var = variable_parse(Term, i);
            ++term_id;
            node[term_id] = var;
    }
    return term_id;
}
void get_leaves_and_internals(int size, int* left, int* right, int*& leaves, 
        int *number_of_leaves){
    (*number_of_leaves) = 0;
    for (int i=0; i<size; ++i){
        if (left[i] == right[i] && left[i] == -1){
            ++(*number_of_leaves);
        }
    }
   
    leaves = (int*)malloc((*number_of_leaves)*sizeof(int));
    int le=0;
    for (int i=0; i<size; ++i){
        if (left[i] == right[i] && left[i] == -1){
            leaves[le] = i;
            ++le;
        }
    }
    return;
}
void read_lambda_term(int root, int* node, int* up, int* left, int* right){
    if (root == -1){
        return;
    }
    debugRead("node[%d]=%d: ", root, node[root]);
    if (up[root] != -1){
        debugRead("up node[%d]=%d, ", up[root], node[up[root]]);
    }else{
        debugRead("up node = -1, ");
    }
    if (left[root] != -1){
        //        debugRead("left node[%d]=%d, ", root, left[root]);
        debugRead("left node[%d]=%d, ", left[root], node[left[root]]);
    }else{
        debugRead("left node = -1, ");
    }
    if (right[root] != -1){
        //        debugRead("right node[%d]=%d\n", root, right[root]);
        debugRead("right node[%d]=%d\n", right[root], node[right[root]]);
    }else{
        debugRead("right node = -1\n");
    }
    read_lambda_term(left[root], node, up, left, right);
    read_lambda_term(right[root], node, up, left, right);
}
void wypisz_term(int root, int* node, int* up, int* left, int* right){
    if (root == -1){
        return;
    }
    if (node[root] == 0){
        //application
        printf("(");
        wypisz_term(left[root], node, up, left, right);
        wypisz_term(right[root], node, up, left, right);
        printf(")");
    }else if (left[root] >= 0 && right[root] == -1){
        wypisz_term(left[root], node, up, left, right);
    }else if (node[root] == -2){
        printf("K");
    }else if (node[root] == -3){
        printf("S");
    }
}
/*
bool isLeaf(int i, int* left, int* right) {
    return left[i] == -1 && right[i] == -1;
}
int getMaxLeafValue(int rozmiar, int* nodes, int* left, int* right) {
    int maxLeaf = -1;
    for (int it = 0; it < rozmiar; ++it) {
        if (isLeaf(it, left, right)) {
            maxLeaf = max(maxLeaf, nodes[it]);
        }
    }
    if (maxLeaf == -1) {
        debugSize("no leaves", __LINE__);
        exit(-1);
    }
}
void shareLeaf(int leaf, int node, int leafIndex, int* left, int* right, vector<int>* up) {
    bool alreadyAdded = false;
    if (left[node] == leaf) {
        left[node] = leafIndex;
        up[leafIndex].push_back(node);
        alreadyAdded = true;
    }
    if (right[node] == leaf) {
        right[node] = leafIndex;
        if (!alreadyAdded) {
            up[leafIndex].push_back(node);
        }
    }
}
int convertToSharedDag(int rozmiar, int* nodes, int* left, int* right, vector<int>* up) {
    map<int, int> vars;
    int maxLeafValue = getMaxLeafValue(rozmiar, nodes, left, right);
    // int fathers[][] = new fathers[] [maxLeafValue];
    for (int it = 0; it < rozmiar; ++it) {
        if (!isLeaf(it, left, right)) {
            continue;
        }
        int parentIndex = up[it][0];
        int sharedLeaf = it;
        if (vars.find(node[it]) == map::end) {
            vars[node[it]] = it;
        } else {
            sharedLeaf = vars[node[it]];
        }
        shareLeaf(it, parentIndex, sharedLeaf, left, right, up);
    }
}
*/

